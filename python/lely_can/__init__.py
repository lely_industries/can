"""Python bindings for the Lely CAN library."""

__copyright__ = '2018 Lely Industries N.V.'
__author__ = 'J. S. Seldenthuis <jseldenthuis@lely.com>'
__version__ = '1.4.0'
__license__ = 'Apache-2.0'

from lely_can.can import *
from lely_can.msg import *

